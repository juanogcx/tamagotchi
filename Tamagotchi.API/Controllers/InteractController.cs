﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Tamagotchi.Data;
using Tamagotchi.Entities;
using Tamagotchi.Services.Interfaces;

namespace Tamagotchi.Controllers
{
    [Produces("application/json")]
    [Route("api/interact")]
    public class InteractController : Controller
    {
        private readonly ITamagotchiService _tamagotchiService;

        public InteractController(ITamagotchiService tamagotchiService)
        {
            _tamagotchiService = tamagotchiService;
        }
        [HttpPost("{id}")]
        public async Task<IActionResult> Interact([FromRoute] int id, [FromBody] PetInteraction petInteraction)
        {
            
            try
            {
                await _tamagotchiService.InteractWithPet(id, petInteraction.InteractionTypeId);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok();
        }
    }
}