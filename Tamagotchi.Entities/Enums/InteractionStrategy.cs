﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tamagotchi.Entities.Enums
{
    public enum InteractionStrategy
    {
        Increase,
        Decrease
    }
}
