﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tamagotchi.Entities
{
    public class Pet
    {
        public int PetId { get; set; }
        public string Name { get; set; }
        public virtual PetType PetType { get; set; }
        public int PetTypeId { get; set; }
        public virtual Person Person { get; set; }
        public int PersonId { get; set; }
        public virtual IEnumerable<PetStatus> Status { get; set; }
    }
}
