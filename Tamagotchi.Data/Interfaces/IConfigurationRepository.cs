﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tamagotchi.Entities;

namespace Tamagotchi.Data.Interfaces
{
    public interface IConfigurationRepository
    {
        Task<PetTypeConfig> GetConfigForInteractionForPet(int petType, int interactionType);
        Task<IEnumerable<InteractionType>> GetInteractionTypes();
    }
}
