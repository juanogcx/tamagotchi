﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Tamagotchi.Data;
using Tamagotchi.Entities;
using Tamagotchi.Services.Interfaces;

namespace Tamagotchi.Controllers
{
    [Produces("application/json")]
    [Route("api/pets")]
    public class PetsController : Controller
    {
        private readonly ITamagotchiService _tamagotchiService;

        public PetsController(ITamagotchiService tamagotchiService)
        {
            _tamagotchiService = tamagotchiService;
        }

        // GET: api/Pets
        [HttpGet]
        public async Task<IActionResult> GetPetsAsync()
        {
            var pets = await _tamagotchiService.GetPets();
            return Ok(pets);
        }

        // GET: api/Pets/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPet([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var pet = await _tamagotchiService.GetPet(id);

            if (pet == null)
            {
                return NotFound();
            }

            return Ok(pet);
        }
    }
}