﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hangfire;
using Microsoft.AspNetCore.Mvc;
using Tamagotchi.Services.Interfaces;

namespace Tamagotchi.Controllers
{
    public class ProcessorController : Controller
    {
        private readonly ITamagotchiProcessor _tamagotchiProcessor;

        public ProcessorController(ITamagotchiProcessor tamagotchiProcessor)
        {
            _tamagotchiProcessor = tamagotchiProcessor;
        }
        public IActionResult Start()
        {
            RecurringJob.AddOrUpdate("100", () => _tamagotchiProcessor.Process(), Cron.Minutely);
            return Content("Processor Started");
        }
        public IActionResult Stop()
        {
            RecurringJob.RemoveIfExists("100");
            return Content("Processor Stopped");
        }
        public async Task<IActionResult> ExecuteManual()
        {
            await _tamagotchiProcessor.Process();
            return Content("Processor Executed");
        }
    }
}