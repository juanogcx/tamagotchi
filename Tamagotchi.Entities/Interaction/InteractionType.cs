﻿using System;
using System.Collections.Generic;
using System.Text;
using Tamagotchi.Entities.Enums;

namespace Tamagotchi.Entities
{
    public class InteractionType
    {
        public int InteractionTypeId { get; set; }
        public string Name { get; set; }
        public InteractionStrategy InteractionStrategy { get; set; }
    }
}
