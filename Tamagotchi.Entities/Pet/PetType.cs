﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tamagotchi.Entities
{
    public class PetType
    {
        public int PetTypeId { get; set; }
        public string Name { get; set; }
        public virtual IEnumerable<PetTypeConfig> Config { get; set; }
    }
}
