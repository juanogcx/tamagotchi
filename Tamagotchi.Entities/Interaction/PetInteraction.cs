﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tamagotchi.Entities
{
    public class PetInteraction
    {
        [JsonIgnore]
        public int PetInteractionId { get; set; }
        [JsonIgnore]
        public InteractionType InteractionType { get; set; }
        public int InteractionTypeId { get; set; }
        [JsonIgnore]
        public Pet Pet { get; set; }
        [JsonIgnore]
        public bool FromServer { get; set; }
    }
}
