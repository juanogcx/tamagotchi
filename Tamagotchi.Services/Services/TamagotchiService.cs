﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tamagotchi.Data.Interfaces;
using Tamagotchi.Entities;
using Tamagotchi.Services.Interfaces;

namespace Tamagotchi.Services.Services
{
    public class TamagotchiService : ITamagotchiService
    {
        private readonly ITamagotchiRepository _tamagotchiRepository;
        private readonly IConfigurationRepository _configurationRepository;

        public TamagotchiService(
            ITamagotchiRepository tamagotchiRepository,
            IConfigurationRepository configurationRepository)
        {
            _tamagotchiRepository = tamagotchiRepository;
            _configurationRepository = configurationRepository;
        }
        public async Task InteractWithPet(int petId, int interactionType)
        {
            //get pet
            var pet = await GetPet(petId);
            if (pet == null)
                throw new Exception("Pet not found. Cannot interact with Pet");

            var config = await _configurationRepository.GetConfigForInteractionForPet(pet.PetTypeId, interactionType);

            var status = await GetPetStatus(pet, interactionType, config);

            if (config.InteractionType.InteractionStrategy == Entities.Enums.InteractionStrategy.Increase)
            {
                if (status.Value > config.MinLevel)
                    status.Value -= 1;
            }
            if (config.InteractionType.InteractionStrategy == Entities.Enums.InteractionStrategy.Decrease)
            {
                if (status.Value < config.MaxLevel)
                    status.Value += 1;
            }

            await SaveStatus(status, false);

        }

        public async Task SaveStatus(PetStatus status, bool FromServer)
        {
            //add interaction as history of operations
            var interaction = new PetInteraction
            {
                InteractionType = status.InteractionType,
                Pet = status.Pet,
                FromServer = FromServer
            };

            await _tamagotchiRepository.AddInteraction(interaction);

            await _tamagotchiRepository.SavePetStatus(status);
        }

        public async Task<PetStatus> GetPetStatus(Pet pet, int interactionType, PetTypeConfig config)
        {
            var status = await _tamagotchiRepository.GetPetStatus(pet.PetId, interactionType);

            if (status == null)
            {
                //create a new status for pet based on default config
                status = new PetStatus
                {
                    InteractionType = config.InteractionType,
                    Pet = pet,
                    Value = config.StartLevel //set an initial value based on configuration for each pet
                };

                return await _tamagotchiRepository.SavePetStatus(status);
            }

            return status;
        }

        public async Task<IEnumerable<Pet>> GetPets()
        {
            return await _tamagotchiRepository.GetAll();
        }
        public async Task<Pet> GetPet(int id)
        {
            return await _tamagotchiRepository.Get(id);
        }
    }
}
