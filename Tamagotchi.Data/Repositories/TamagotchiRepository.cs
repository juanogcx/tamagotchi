﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tamagotchi.Data.Interfaces;
using Tamagotchi.Entities;

namespace Tamagotchi.Data.Repositories
{
    public class TamagotchiRepository: ITamagotchiRepository
    {
        private readonly ApplicationDbContext _context;
        public TamagotchiRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Pet>> GetAll()
        {
            return await _context.Pets
                .Include(p => p.PetType)
                .Include(p => p.Person)
                .Include(p => p.Status)
                .Include("PetType.Config")
                .ToListAsync();
        }

        public async Task<Pet> Get(int id)
        {
            return await _context.Pets.Include(p=>p.Person).SingleOrDefaultAsync(m => m.PetId == id);
        }

        public async Task<PetStatus> GetPetStatus(int id, int interactionType)
        {
            return await _context.PetStatuses.Include(p => p.Pet)
                .SingleOrDefaultAsync(m => m.Pet.PetId == id && m.InteractionType.InteractionTypeId == interactionType);
        }
        public async Task<PetStatus> SavePetStatus(PetStatus petStatus)
        {
            if (petStatus.PetStatusId == 0)
                _context.PetStatuses.Add(petStatus);
            else
                _context.Entry(petStatus).State = EntityState.Modified;

            await _context.SaveChangesAsync();

            return petStatus;
        }

        public async Task AddInteraction(PetInteraction interaction)
        {
            _context.PetInteractions.Add(interaction);
            await _context.SaveChangesAsync();
        }
    }
}
