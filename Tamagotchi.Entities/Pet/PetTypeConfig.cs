﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tamagotchi.Entities
{
    public class PetTypeConfig
    {
        public int PetTypeConfigId { get; set; }
        public int MaxLevel { get; set; }
        public int MinLevel { get; set; }
        public int StartLevel { get; set; }
        public int WarningLevel { get; set; }
        public int TimeToChangeAPoint { get; set; }
        public virtual InteractionType InteractionType { get; set; }
        public int InteractionTypeId { get; set; }
        public virtual PetType PetType { get; set; }
        public int PetTypeId { get; set; }
    }
}
