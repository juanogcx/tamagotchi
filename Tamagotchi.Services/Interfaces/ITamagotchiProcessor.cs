﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Tamagotchi.Services.Interfaces
{
    public interface ITamagotchiProcessor
    {
        Task Process();
    }
}
