﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tamagotchi.Data.Interfaces;
using Tamagotchi.Entities;
using Microsoft.EntityFrameworkCore;

namespace Tamagotchi.Data.Repositories
{
    public class ConfigurationRepository : IConfigurationRepository
    {
        private readonly ApplicationDbContext _context;
        public ConfigurationRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<PetTypeConfig> GetConfigForInteractionForPet(int petType, int interactionType)
        {
            return await _context.PetTypeConfigs.Include(p=>p.InteractionType)
                .FirstOrDefaultAsync(p => p.InteractionTypeId == interactionType && p.PetTypeId == petType);
        }
        public async Task<IEnumerable<InteractionType>> GetInteractionTypes()
        {
            return await _context.InteractionTypes.ToListAsync();
        }
    }
}
