import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-my_pets',
  templateUrl: './my_pets.component.html',
  styleUrls: ['./my_pets.component.css']
})
/** my_pets component*/
export class MyPetsComponent {

  public pets: Pet[];
  http: HttpClient;

  getData() {
    this.http.get<Pet[]>('http://localhost:60115/api/Pets').subscribe(result => {
      this.pets = result;
    }, error => console.error(error));
  }

  public interact(actionId: number, petId: number) {
    let interaction = new Interaction();
    interaction.InteractionTypeId = actionId;
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    this.http.post('http://localhost:60115/api/interact/' + petId, interaction)
      .subscribe(result => {
        this.getData();
    }, error => console.error(error));
  }

  constructor(http: HttpClient) {
    this.http = http;
    this.getData();
    setInterval(() => {
      this.getData();
    }, 5000);
  }
}

class Interaction {
  PetInteractionId: number;
  InteractionTypeId: number
}

interface Config {
  name: string;
  maxLevel: number;
  minLevel: number;
}

interface PetType {
  name: string;
  config: Config[];
}

interface Status {
  value: number;
}

interface Person {
  name: string;
}

interface Pet {
  petId: number;
  name: string;
  petType: PetType;
  status: Status[];
  person: Person;
}

