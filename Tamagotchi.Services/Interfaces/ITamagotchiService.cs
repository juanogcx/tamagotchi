﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tamagotchi.Entities;

namespace Tamagotchi.Services.Interfaces
{
    public interface ITamagotchiService
    {
        Task InteractWithPet(int petId, int interactionType);
        Task<IEnumerable<Pet>> GetPets();
        Task<Pet> GetPet(int id);
        Task<PetStatus> GetPetStatus(Pet pet, int interactionType, PetTypeConfig config);
        Task SaveStatus(PetStatus status, bool FromServer);
    }
}
