﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tamagotchi.Data.Interfaces;
using Tamagotchi.Services.Interfaces;

namespace Tamagotchi.Services.Services
{
    public class TamagotchiProcessor: ITamagotchiProcessor
    {
        private readonly ITamagotchiService _tamagotchiService;
        private readonly IConfigurationRepository _configurationRepository;

        public TamagotchiProcessor(ITamagotchiService tamagotchiService, IConfigurationRepository configurationRepository)
        {
            _tamagotchiService = tamagotchiService;
            _configurationRepository = configurationRepository;
        }

        public async Task Process()
        {
            var pets = await _tamagotchiService.GetPets();

            var interactionTypes = await _configurationRepository.GetInteractionTypes();

            foreach (var pet in pets)
            {
                //maybe this can be splitted out in n different services to process each interaction. Doing this 
                //doing this for simplicity
                //Ex. 1 service for petting 1 service for feeding. etc. 
                foreach (var interaction in interactionTypes)
                {
                    var config = await _configurationRepository.GetConfigForInteractionForPet(pet.PetTypeId, interaction.InteractionTypeId); //just for simplicity

                    if (config == null) //config not found for this type of pet. Just log and continue
                        continue;

                    var petStatus = await _tamagotchiService.GetPetStatus(pet, interaction.InteractionTypeId, config);

                    if (petStatus.LastRun != null 
                        && !(petStatus.LastRun.Value.AddMinutes(config.TimeToChangeAPoint) < DateTime.Now))
                        continue; //don't consume points yet

                    //check strategy, the processor is the evil here
                    if(config.InteractionType.InteractionStrategy == Entities.Enums.InteractionStrategy.Increase)
                    {
                        if(petStatus.Value < config.MaxLevel)
                            petStatus.Value += 1;
                    }
                    if (config.InteractionType.InteractionStrategy == Entities.Enums.InteractionStrategy.Decrease)
                    {
                        if (petStatus.Value > config.MinLevel)
                            petStatus.Value -= 1;
                    }

                    if(petStatus.Value == config.WarningLevel)
                    {
                        //TODO: Send a notification to the user, the pet is in warning level on one of the interactions
                    }

                    petStatus.LastRun = DateTime.Now;

                    await _tamagotchiService.SaveStatus(petStatus, true);
                }
            }
        }
    }
}
