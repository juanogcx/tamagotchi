﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tamagotchi.Entities;

namespace Tamagotchi.Data.Interfaces
{
    public interface ITamagotchiRepository
    {
        Task<IEnumerable<Pet>> GetAll();
        Task<Pet> Get(int id);
        Task<PetStatus> GetPetStatus(int id, int interactionType);
        Task<PetStatus> SavePetStatus(PetStatus petStatus);
        Task AddInteraction(PetInteraction interaction);
    }
}
