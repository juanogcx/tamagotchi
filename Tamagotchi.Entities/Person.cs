﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tamagotchi.Entities
{
    public class Person
    {
        public int PersonId { get; set; }
        public string Name { get; set; }
    }
}
