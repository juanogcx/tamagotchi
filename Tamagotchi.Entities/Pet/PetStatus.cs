﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tamagotchi.Entities
{
    public class PetStatus
    {
        public int PetStatusId { get; set; }
        public InteractionType InteractionType { get; set; }
        public Pet Pet { get; set; }
        public int Value { get; set; }
        public DateTime? LastRun { get; set; }
    }
}
