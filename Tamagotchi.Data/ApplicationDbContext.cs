﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Tamagotchi.Entities;

namespace Tamagotchi.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Tamagotchi.Entities.Pet> Pets { get; set; }
        public DbSet<Tamagotchi.Entities.PetType> PetTypes { get; set; }
        public DbSet<Tamagotchi.Entities.PetInteraction> PetInteractions { get; set; }
        public DbSet<Tamagotchi.Entities.InteractionType> InteractionTypes { get; set; }
        public DbSet<Tamagotchi.Entities.Person> Persons { get; set; }
        public DbSet<Tamagotchi.Entities.PetStatus> PetStatuses { get; set; }
        public DbSet<Tamagotchi.Entities.PetTypeConfig> PetTypeConfigs { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
           : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);

            builder.Entity<InteractionType>().HasData(new InteractionType
            {
                Name = "Feeding",
                InteractionTypeId = 1,
                InteractionStrategy = Entities.Enums.InteractionStrategy.Increase
            });
            builder.Entity<InteractionType>().HasData(new InteractionType
            {
                Name = "Petting",
                InteractionTypeId = 2,
                InteractionStrategy = Entities.Enums.InteractionStrategy.Decrease
            });


            builder.Entity<PetType>().HasData(new PetType { Name = "Electric", PetTypeId = 1 });
            builder.Entity<PetType>().HasData(new PetType { Name = "Fire", PetTypeId = 2 });
            builder.Entity<PetType>().HasData(new PetType { Name = "Ghost", PetTypeId = 3 });
            builder.Entity<PetType>().HasData(new PetType { Name = "Fire", PetTypeId = 4 });
            
            //Feeding config for Electric
            builder.Entity<PetTypeConfig>().HasData(new PetTypeConfig
            {
                PetTypeConfigId = 1,
                PetTypeId = 1,
                InteractionTypeId = 1,
                TimeToChangeAPoint = 2,
                MaxLevel = 40,
                MinLevel = 0,
                StartLevel = 10,
                WarningLevel = 25,
            });

            //Petting config for Electric
            builder.Entity<PetTypeConfig>().HasData(new PetTypeConfig
            {
                PetTypeConfigId = 2,
                PetTypeId = 1,
                InteractionTypeId = 2,
                TimeToChangeAPoint = 3,
                MaxLevel = 50,
                MinLevel = 10,
                StartLevel = 40,
                WarningLevel = 20,
            });

            //Feeding config for Fire
            builder.Entity<PetTypeConfig>().HasData(new PetTypeConfig
            {
                PetTypeConfigId = 3,
                PetTypeId = 2,
                InteractionTypeId = 1,
                TimeToChangeAPoint = 4,
                MaxLevel = 60,
                MinLevel = 0,
                StartLevel = 15,
                WarningLevel = 30,
            });

            //Petting config for Fire
            builder.Entity<PetTypeConfig>().HasData(new PetTypeConfig
            {
                PetTypeConfigId = 4,
                PetTypeId = 2,
                InteractionTypeId = 2,
                TimeToChangeAPoint = 5,
                MaxLevel = 30,
                MinLevel = 5,
                StartLevel = 20,
                WarningLevel = 10,
            });

            builder.Entity<Person>().HasData(new Person { Name = "Larry", PersonId = 1 });
            builder.Entity<Person>().HasData(new Person { Name = "Samus", PersonId = 2 });

            builder.Entity<Pet>().HasData(new Pet { Name = "Pikachu", PetId = 1, PetTypeId = 1, PersonId = 1 });
            builder.Entity<Pet>().HasData(new Pet { Name = "Torchic", PetId = 2, PetTypeId = 2, PersonId = 2 });
            builder.Entity<Pet>().HasData(new Pet { Name = "Mimikyu", PetId = 3, PetTypeId = 3, PersonId = 1 });
            builder.Entity<Pet>().HasData(new Pet { Name = "Charizard", PetId = 4, PetTypeId = 4, PersonId = 1 });
            builder.Entity<Pet>().HasData(new Pet { Name = "Moltres", PetId = 5, PetTypeId = 4, PersonId = 2 });
        }
    }
}
